#include <FHT.h>

#include <FastLED.h>

int r = 0;
int g = 0;
int b = 0;

#define LED_PIN 8
#define NUM_LEDS 60 
#define BRIGHTNESS 50

CRGB leds[NUM_LEDS]; 
CRGB led[NUM_LEDS]; 
int s=0;

void setup() { 
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS); 
  FastLED.setBrightness(BRIGHTNESS);
  for (int i = 0; i <= NUM_LEDS; i++){  
    leds[i] = CRGB (r,g,b ); 
    delay(40);
    FastLED.show(); 
  } 
  Serial.begin(57600); 
  pinMode(A0,INPUT); 
  }

void loop(){ 
  s=analogRead(A0); 
  Serial.println(s);
  for(int i = 0; i <=20;i++){
    if(s > 540){
        leds[i] = CRGB(0,0,100);
        FastLED.show();
        if (s > 570){
            leds[i+20] = CRGB(100,100,0);
            FastLED.show();
            if (s > 590){
                leds[i+40] = CRGB(100,0,0);
                FastLED.show();
            }
            else{
                leds[59-i] = CRGB(0,0,0); 
                FastLED.show();
              } 
          }
        else{
          leds[40-i] = CRGB(0,0,0);
          FastLED.show();

          }
         }
    else{
      leds[20-i] = CRGB(0,0,0);
      FastLED.show();
      }
}

delay(1);
} 
