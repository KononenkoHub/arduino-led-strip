#include <FastLED.h>

int r = 0;
int g = 0;
int b = 0;

#define LED_PIN 8
#define NUM_LEDS 60 
#define BRIGHTNESS 50

CRGB leds[NUM_LEDS]; 
CRGB led[NUM_LEDS]; 
int s=0;

void setup() { 
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS); 
  FastLED.setBrightness(BRIGHTNESS);
  for (int i = 0; i <= NUM_LEDS; i++){  
    leds[i] = CRGB (r,g,b ); 
    delay(40);
    FastLED.show(); 
  } 
  Serial.begin(57600); 
  pinMode(A0,INPUT); 
  }

void loop(){ 
 for(int i =0; i< NUM_LEDS;i++){
  leds[i] = CRGB(150,125,0);
  FastLED.show();
  delay(40);
  }

  for(int i =NUM_LEDS; i>0;i--){
  leds[i] = CRGB(0,0,0);
  FastLED.show();
  delay(40);
  }
} 
