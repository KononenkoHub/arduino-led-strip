#include <FastLED.h>

CRGB defaultColor = CRGB(0, 0, 0);

#define LED_PIN 8
#define NUM_LEDS 30 
#define SegmentsCount 5

CRGB leds[NUM_LEDS]; 
CRGB gradientLeds[NUM_LEDS];
float prevColorBuffer[NUM_LEDS];
float blendStep = 255.0 / 30.0;

int ledsBySegment = (NUM_LEDS/2) / SegmentsCount;
CRGB segmentsColor[SegmentsCount] = {CRGB(255, 0, 255), CRGB(0, 0, 255), CRGB(0, 255, 0), CRGB(252, 55, 5), CRGB(255, 0, 0)};

CRGB ApplyPreviousColors(int index, CRGB current){
  float value = prevColorBuffer[index];

  CRGB result = blend(current, defaultColor, value);
}

CRGB GetColorByIndex(int index){
  return gradientLeds[index];
}

void setup() { 
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS); 
  FastLED.setBrightness(255);
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i] = defaultColor;
    prevColorBuffer[i] = 1;
  }  

  for(int i = 1; i < SegmentsCount + 1; i++){
    fill_gradient_RGB(gradientLeds, (i - 1) * ledsBySegment, segmentsColor[i - 1], i * ledsBySegment, segmentsColor[i]);
  }
  FastLED.show();
  Serial.begin(115200); 
  
  pinMode(A0,INPUT); 
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A4, OUTPUT);
  digitalWrite(A1, LOW);
  digitalWrite(A2, HIGH);
  digitalWrite(A4, LOW);
}

void loop(){ 
  float volume_norm = analogRead(A0) / 512.0;
  int audio_l = analogRead(A3) * volume_norm;
  int audio_r = analogRead(A5) * volume_norm;
  int temp_index = round((audio_l + audio_r) / 2.0) / 5;
  temp_index = (constrain(temp_index, 0, NUM_LEDS - 1) / 2);
  
  for(int i = temp_index + 1; i < NUM_LEDS; i++){
    prevColorBuffer[i] += blendStep;
    prevColorBuffer[i] = constrain(prevColorBuffer[i], 0, 255);
    leds[i+15] = ApplyPreviousColors(i+15, GetColorByIndex(i));
  }
  
  for(int i = 0; i <= temp_index; i++){
    prevColorBuffer[i+15] = 0;
    leds[i+15] = ApplyPreviousColors(i+15, GetColorByIndex(i));
  }
  for(int i = 0; i < NUM_LEDS / 2; i++){
    leds[14 - i] = leds[i + 15];
  }
  
  FastLED.show(); 
  delay(5);
}
