#include <fix_fft.h>
#include <FastLED.h>

CRGB defaultColor = CRGB(0, 0, 0);

#define LED_PIN 8
#define NUM_LEDS 60 

CRGB leds[NUM_LEDS]; 
CRGB led[NUM_LEDS]; 
char im[128], data[128];
int i = 0, val;

void setup() { 
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS); 
  for (int i = 0; i < 60; i++){ 
    leds[i] = defaultColor; 
    delay(10);
    FastLED.show(); 
  } 
  pinMode(A0,INPUT); 
  Serial.begin(9600);
  }

CRGB startColor = CRGB(230, 3, 250);
CRGB endColor = CRGB(255, 0, 0);


CRGB ColorLerp(CRGB a, CRGB b, float t){
  int ri = IntLerp(a.red, b.red, t);
  int gi = IntLerp(a.green, b.green, t);
  int bi = IntLerp(a.blue, b.blue, t);

  return CRGB(ri, gi, bi);
}

int IntLerp(int a, int b, float t)
{
  t = t < 0 ? 0 : t > 1 ? 1 : t;
  return a + round((b - a) * t);
}
void loop(){   
  float volume_norm = analogRead(A0) / 512.0;
  int audio_l = analogRead(A3) * volume_norm;
  int audio_r = analogRead(A5) * volume_norm;
  
  int audio = round((audio_l + audio_r) / 2.0);
  
  int min=1024, max=0;
  for (i = 0; i < 128; i++) {
    data[i] = audio;
    im[i] = 0;
    if(audio>max) max=audio;
    if(audio<min) min=audio;
  }
  fix_fft(data, im, 4, 0); 

  for(int i = 0; i < 30; i++){
    leds[i] = defaultColor;
  }

  int lowFreqAmpl = sqrt(data[0] * data[0] + im[0] * im[0]) / 4;
  
  for (i = 0; i < lowFreqAmpl; i++) {
    leds[i] = CRGB(100, 0, 0);
  }
  Serial.println(lowFreqAmpl);
  
  FastLED.show(); 
  delay(15); 
}
